"""
Variable, get_variable, sharing
"""

"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

import os
import pprint
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

pp = pprint.PrettyPrinter(indent=4)

x = tf.Variable(10)
y = tf.Variable(20)

print('\nGlobal Variables (step 0)')
pp.pprint(tf.global_variables())

tf.reset_default_graph()

x = tf.Variable(10, name='x')
y = tf.Variable(20, name='y')

print('\nGlobal Variables (step 1)')
pp.pprint(tf.global_variables())

x = tf.Variable(10, name='x')
y = tf.Variable(20, name='y')

print('\nGlobal Variables (step 2)')
pp.pprint(tf.global_variables())

tf.reset_default_graph()

print('\nGlobal Variables (step 3)')
pp.pprint(tf.global_variables())

x = tf.get_variable('x', initializer=10, dtype=tf.int32)
y = tf.get_variable('y', initializer=20, dtype=tf.int32)

print('\nGlobal Variables (step 4)')
pp.pprint(tf.global_variables())

try:
    # This will NOT work
    x = tf.get_variable('x', initializer=10, dtype=tf.int32)
    y = tf.get_variable('y', initializer=20, dtype=tf.int32)
except Exception as e:
    print()
    print(e)

tf.get_variable_scope().reuse_variables()

x1 = tf.get_variable('x', initializer=10, dtype=tf.int32)
y1 = tf.get_variable('y', initializer=20, dtype=tf.int32)

print()
print('x == x1 : {0}'.format(x == x1))
print('y == y1 : {0}'.format(y == y1))

print('\nGlobal Variables (step 5)')
pp.pprint(tf.global_variables())


"""
Linear regression as stochastic gradient descent
"""

"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

import os
import time
import warnings

import matplotlib.pyplot as plt
import tensorflow as tf

import utils

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.simplefilter("ignore")

data_filename = 'data/birth_life_2010.txt'
learning_rate = 0.001

# read in data from the .txt file
data, n_samples = utils.read_data_from_txt(data_filename)

# create placeholders for X (birth rate) and Y (life expectancy)
x = tf.placeholder(tf.float32, name='x')
y = tf.placeholder(tf.float32, name='y')

# create weight and bias, initialized to 0
w = tf.get_variable('weights', initializer=tf.constant(0.0))
b = tf.get_variable('bias', initializer=tf.constant(0.0))

# Representation: linear model
y_predicted = w * x + b

# Evaluation: use square error
loss = tf.square(y - y_predicted, name='loss')

wGrad, bGrad = tf.gradients(loss, [w, b])

# increment assignments must be made within the graph
wIncrement = tf.assign(w, w - learning_rate * wGrad, name='wIncrement')
bIncrement = tf.assign(b, b - learning_rate * bGrad, name='bIncrement')

# interactive plot on
plt.ion()

start = time.time()
writer = tf.summary.FileWriter('./graphs/linear_regression', tf.get_default_graph())
with tf.Session() as sess:
    # initialize variables, in this case, W and b
    sess.run(tf.global_variables_initializer())

    # Optimization: train the model for 100 epochs with SGD
    for j in range(100):
        total_loss = 0
        # run one epoch
        for x_i, y_i in data:
            # execute one SGD step and fetch value of loss
            _, _, l = sess.run([wIncrement, bIncrement, loss], feed_dict={x: x_i, y: y_i})
            total_loss += l
        print('Epoch {0}: {1}'.format(j, total_loss / n_samples))

        # compute parameter values
        w_out, b_out = sess.run([w, b])

        # plot the results
        plt.cla()
        plt.title('Epoch {0}'.format(j))
        plt.ylim([40, 85])
        plt.plot(data[:, 0], data[:, 1], 'bo', label='Real data')
        plt.plot(data[:, 0], data[:, 0] * w_out + b_out, 'r', label='Predicted data')
        plt.legend()
        plt.show()
        plt.pause(0.0001)

    writer.close()

print('Took: {0} seconds'.format(time.time() - start))

# interactive plot off
plt.ioff()
plt.show()

"""
First create the graph then start a session
"""

"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

import os
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

x = tf.Variable(10, name='x')
y = tf.Variable(20, name='y')
z = tf.assign(x, tf.add(x, y))

writer = tf.summary.FileWriter('graphs/off_session', tf.get_default_graph())
writer.close()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # The graph will be shown in TensorBoard 'as is'
    writer = tf.summary.FileWriter('graphs/graph_then_session', sess.graph)
    print('normal')
    for _ in range(10):
        print(sess.run(z))
    writer.close()

# Reset graph to start anew
tf.reset_default_graph()

x = tf.Variable(10, name='x')
y = tf.Variable(20, name='y')

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # Do not pass sess.graph here, since the graph will be expanded below
    writer = tf.summary.FileWriter('graphs/graph_in_session')
    print('inline')
    for _ in range(10):
        print(sess.run(tf.assign(x, tf.add(x, y))))
    # Pass sess.graph here, instead
    writer.add_graph(sess.graph)
    writer.close()


"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

import itertools
import os
import warnings

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.contrib.keras.api.keras.models import model_from_json
from tensorflow.contrib.keras.api.keras.preprocessing.image import ImageDataGenerator


def confusion_matrix(y_valid, y_pred):
    classes = max(np.max(y_valid), np.max(y_pred)) + 1
    matrix = np.zeros((classes, classes), dtype=int)

    for i, j in zip(y_valid, y_pred):
        matrix[i, j] += 1

    return matrix


def plot_confusion_matrix(y_valid, y_pred):
    assert len(y_valid) == len(y_pred)

    color_labels = ['is_Cat', 'is_Dog']
    num_data = len(y_valid)
    expected_labels = [y_valid[i] for i in range(num_data)]
    predicted_labels = [int(y_pred[i] > 0.5) for i in range(num_data)]

    cm = confusion_matrix(expected_labels, predicted_labels)
    plt.figure(figsize=(6, 6))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.copper)
    plt.colorbar()
    tick_marks = np.arange(len(color_labels))
    plt.xticks(tick_marks, color_labels, rotation=90)
    plt.yticks(tick_marks, color_labels)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    return None


# for deterministic repeatability (since a Keras generator are used)
np.random.seed(1)
tf.set_random_seed(2)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
warnings.simplefilter("ignore")

# set experimental parameters
img_width, img_height = 299, 299

new_extended_inception_network = '07_inception_v3_fine_tuning_network.json'
new_extended_inception_weights = '07_inception_v3_fine_tuning_final_weights.hdf5'

test_data_dir = 'data/dogs_and_cats/test'

nb_test_samples = 800

batch_size = 24

# load json and re-create model
with open(os.path.join('models', new_extended_inception_network), 'r') as json_file:
    model_json = json_file.read()

model = model_from_json(model_json)
print("Loaded network model from json file")

# load weights into new model
model.load_weights(os.path.join('models', new_extended_inception_weights))
print("Loaded network weights from hdf5 file.")

# prepare data augmentation configuration
test_datagen = ImageDataGenerator(rescale=1. / 255)

test_generator = test_datagen.flow_from_directory(
    test_data_dir,
    shuffle=False,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='categorical')

# prediction can be done without compiling the model
print('Computing predictions on test set ...')
predictions = model.predict_generator(test_generator, steps=nb_test_samples / batch_size)
print('Done.')

# Ground truth classes are stored in the Keras generator (after loading)
y = test_generator.classes

# Turn predictions into classes
y_predicted = np.argmax(predictions, axis=1)

plot_confusion_matrix(y, y_predicted)
plt.show(block=True)

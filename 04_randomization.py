""" Examples to demonstrate ops level randomization
CS 20: "TensorFlow for Deep Learning Research"
cs20.stanford.edu
Chip Huyen (chiphuyen@cs.stanford.edu)
Lecture 05
"""

"""
Adapted by marco.piastra@unipv.it, gianluca.gerard01@universitadipavia.it,
           andrea.pedrini@unipv.it, mirto.musci@unipv.it
"""

import os

import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# no seed
c = tf.random_uniform([], -10, 10)

# outcome is unpredictable
with tf.Session() as sess:
    print('Session 1: no seed, random variable c')
    print(sess.run(c))
    print(sess.run(c))

with tf.Session() as sess:
    print('Session 2: no seed, random variable c')
    print(sess.run(c))
    print(sess.run(c))

# seed in one random generator
c = tf.random_uniform([], -10, 10, seed=2)

with tf.Session() as sess:
    print('Session 3: seed in generator, random variable c')
    print(sess.run(c))
    print(sess.run(c))

# each new session will start the random state all over again.
with tf.Session() as sess:
    print('Session 4: seed in generator, random variable c')
    print(sess.run(c))
    print(sess.run(c))

# with operation level random seed, each op keeps its own seed.
c = tf.random_uniform([], -10, 10, seed=2)
d = tf.random_uniform([], -10, 10, seed=2)

with tf.Session() as sess:
    print('Session 5: seed in generators, random variables c and d')
    print(sess.run(c))
    print(sess.run(d))

# graph level random seed
tf.set_random_seed(2)

c = tf.random_uniform([], -10, 10)
d = tf.random_uniform([], -10, 10)

# outcome is repeatable
with tf.Session() as sess:
    print('Session 6: seed in graph, random variables c and d')
    print(sess.run(c))
    print(sess.run(d))

with tf.Session() as sess:
    print('Session 7: seed in graph, random variables c and d')
    print(sess.run(c))
    print(sess.run(d))
